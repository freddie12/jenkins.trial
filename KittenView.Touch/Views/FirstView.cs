using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.Touch.Views;
using CoreGraphics;
using Foundation;
using ObjCRuntime;
using UIKit;
using KittenView.Core.ViewModels;

namespace KittenView.Touch.Views
{
    [Register("FirstView")]
    public class FirstView : MvxTableViewController
    {
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

			// ios7 layout
            if (RespondsToSelector(new Selector("edgesForExtendedLayout")))
            {
               EdgesForExtendedLayout = UIRectEdge.None;
            }

			var source = new Cirrious.MvvmCross.Binding.Touch.Views.MvxStandardTableViewSource (TableView, "TitleText, Name; ImageUrl ImageUrl");
			TableView.Source = source;

			var set = this.CreateBindingSet<FirstView, FirstViewModel> ();
			set.Bind (source).To (vm => vm.Kittens);
			set.Apply ();

			TableView.ReloadData ();
			   
            
        }
    }
}