using Android.App;
using Cirrious.MvvmCross.Droid.Views;
using Android.OS;
using Android.Support.V4.Widget;
using Cirrious.MvvmCross.Binding.Droid.Views;
using Android.Views;
using System.Threading;
using Android.Widget;
using Android.Content;
using KittenView.Core.ViewModels;
using System.Collections.Generic;

namespace KittenView.Droid.Views
{
	[Activity(MainLauncher = true)]
	public class RefreshView : MvxActivity
	{
		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.RefreshView);
		}
	}

}