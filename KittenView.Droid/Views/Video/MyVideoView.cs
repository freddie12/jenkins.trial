﻿using System;
using Android.Content;
using Android.Util;
using Android.Webkit;
//using FanApp.Droid.Helpers;
using System.IO;
//using FanApp.Core.Services.Impl;
using System.Diagnostics;
using System.Collections.Generic;
//using Newtonsoft.Json;
using Android.Widget;
using Android.Views;
using Android.App;
using Android.Media;
//using System.Net.Http;
using Android.Graphics;
//using FanApp.Core.Services;
using Android.Runtime;
using System.Threading.Tasks;
using Android.Content.PM;
using System.Net.Http;
using Android;

namespace KittenView.Droid.Views.Video
{

	public class MyVideoView : VideoView, 
	MediaPlayer.IOnCompletionListener, 
	MediaPlayer.IOnErrorListener, 
	MediaPlayer.IOnInfoListener, 
	MediaPlayer.IOnPreparedListener,
	MediaPlayer.IOnBufferingUpdateListener,
	ISurfaceHolderCallback

	{
		System.Timers.Timer timer;
		private int lastposition = 0;
		public Activity activity { get; set; }
		public ProgressBar spinnerView { get; set; }
		public FrameLayout videoFrame { get; set; }
		public RelativeLayout iC { get; set; }
		public TextView heading { get; set; }
		public ImageView mainimage { get; set; }
		public TextView noVidsMessage { get; set; }
		public LinearLayout mainLayout { get; set; }
		public Button playbutton { get; set; }
		public ScrollView scrollView { get; set; }
		public string Title { get; set; }
		private MediaPlayer mp;
		private MediaController mc;
		private int _timeoutSecs = 0;
		private int _secondsWithNoPlaybackChange = 0;
		private int _currentPositionSnapshot = 0;
		private bool _videoCheckRunning = false;
		private int mForceHeight = 0;
		private int mForceWidth = 0;
		private String errorMessage;
		private int _howManyErrors = 0;
		private int _maxErrorsBeforeStopping = 5;
		private int _intervalDuration = 0;
		private String _url;

		// http://developer.android.com/reference/android/widget/VideoView.html

		public MyVideoView(Context context, IAttributeSet attrs) : base(context, attrs)
		{
			Init (context);

			Log.Info ("tpsVideoView","Loading");

			try {
				if (timer == null) {
					timer = new System.Timers.Timer ();
					timer.Interval = 2000; 
					timer.Elapsed += OnTimedEvent;
				}

				errorMessage = "Either the video is unavailable or there was a problem connecting to the server. Please check your internet connection and try again";
				mc = new MediaController(context);
				mp = new MediaPlayer();
				_timeoutSecs = (int)Math.Ceiling ((double)(1000 / 2000));
			}
			catch (Exception e) {
				Log.Error ("TPSVideoView",e.Message);
				Log.Error ("TPSVideoView",e.StackTrace);
			}

		}	

		private void Init (Context context)
		{
			activity = (Activity) context;
			spinnerView = new ProgressBar (Context);
			videoFrame = new FrameLayout (Context);
			iC = new RelativeLayout (Context);
			heading = new TextView (Context);
			mainimage = new ImageView (Context);
			noVidsMessage = new TextView (Context);
			playbutton = new Button (Context);
			scrollView = new ScrollView (Context);
		}

		public void startTimer()
		{
			Log.Info ("tpsVideoView","Timer start");
			timer.Enabled = true;

		}

		public void stopTimer()
		{
			Log.Info ("tpsVideoView","Timer stop");
			timer.Enabled = false;
			timer.Close ();

		}


		public void ShowImageNoVideoAvailable(string message)
		{

			if (noVidsMessage != null) {
				noVidsMessage.Text = message;
				noVidsMessage.Visibility = ViewStates.Visible;
			}
			if (playbutton != null) {
				playbutton.Visibility = ViewStates.Gone;
			}
			if (mainimage != null) {
				mainimage.Visibility = ViewStates.Gone;
			}

			if (spinnerView != null) spinnerView.Visibility = ViewStates.Gone;
			if (videoFrame != null) videoFrame.Visibility = ViewStates.Gone;

		}


		public void ShowImage(bool showPlayButton)
		{

			Log.Info ("tpsVideoView","ShowImage called ");

			//stopPlaying (); // <- this causes loopings
			HideSpinner ();
			stopTimer ();
			_howManyErrors = 0; // reset to 0

			if (this != null) {
				this.Visibility = ViewStates.Gone;
				this.Invalidate ();
			}
			if (showPlayButton && playbutton != null)
				playbutton.Visibility = ViewStates.Visible;

			else
				playbutton.Visibility = ViewStates.Gone;

			if (mainimage != null) {
				mainimage.Visibility = ViewStates.Visible;
			}

			if (spinnerView != null) spinnerView.Visibility = ViewStates.Gone;
			if (videoFrame != null) videoFrame.Visibility = ViewStates.Gone;
			if (iC != null) {
				iC.Visibility = ViewStates.Visible;
				iC.Invalidate ();
			}

		}

		public void SetHeading(String title)
		{
			Log.Info ("tpsVideoView","Heading set");
			heading.Text = title;
			iC.Invalidate ();
		}

		public void ShowVideo()
		{
			Log.Info ("tpsVideoView","ShowVideo called ");

			startTimer ();
			HideSpinner ();
			videoFrame.Visibility = ViewStates.Visible;

			if (this != null) {
				this.Visibility = ViewStates.Visible;
				this.Invalidate ();
			}

			if (playbutton != null) {
				playbutton.Visibility = ViewStates.Visible;
			}

			iC.Visibility = ViewStates.Gone;
			iC.Invalidate ();

		}

		public void ShowSpinner()
		{
			Log.Info ("tpsVideoView","ShowSpinner called ");
//			spinnerView.Visibility = ViewStates.Visible;
		}

		public void HideSpinner()
		{
			Log.Info ("tpsVideoView","HideSpinner called ");
//			spinnerView.Visibility = ViewStates.Gone;
		}

		public async Task<Bitmap> GetImageFromUrl(string url)
		{
			Log.Info ("tpsVideoView","GetImageFromurl called ");
			if (url == null)
				return null;

			using(var client = new HttpClient())
			{
				var result = await client.GetAsync(url);
				if (result.IsSuccessStatusCode)
				{
					using(var stream = await result.Content.ReadAsStreamAsync())
					{
						﻿var bitmap = await BitmapFactory.DecodeStreamAsync(stream);
						return bitmap;
					}
				}
			}
			return null;
		}

		public async void UpdateImageAndTitle(string thumbnailurl,string title) 
		{
			Log.Info ("tpsVideoView","UpdateImageAndTitle called ");
			try {
				var bm = await this.GetImageFromUrl (thumbnailurl);   
				if (bm != null) mainimage.SetImageBitmap (bm);
				if (title != null && heading != null) heading.Text = title;
			}
			catch (Exception e) {
				Log.Error("TpsVideoView",e.Message);
				Log.Error("TpsVideoView",e.StackTrace);
			}

		}


		private void OnTimedEvent(object sender, System.Timers.ElapsedEventArgs e)
		{
			Log.Info ("tpsVideoView","TimedEvent called ");
			// reactivate onConfigurationChange
			//http://stackoverflow.com/questions/13073500/onconfigurationchanged-not-called-once-setrequestedconfiguration-has-been-used
			// http://developer.android.com/guide/topics/manifest/activity-element.html#screen
			this.activity.RequestedOrientation = ScreenOrientation.Sensor;

			_currentPositionSnapshot = this.CurrentPosition;

			_intervalDuration = this.CurrentPosition - lastposition;

			// if we don't have an interval of 1000 between timed events then the video is stuttering, we won't go on like this for long

			if (_intervalDuration < 1000)
				_howManyErrors++;
			if (_intervalDuration > 1000 && _howManyErrors > 0)
				_howManyErrors--;


			Log.Info ("tpsVideoView", _url + "Should i still play? " + _secondsWithNoPlaybackChange + ">" + _timeoutSecs + " // " + _howManyErrors + ">" + _maxErrorsBeforeStopping);


			if (_secondsWithNoPlaybackChange > _timeoutSecs || _howManyErrors > _maxErrorsBeforeStopping )  // don't test for playing here as we may not have ever started!
			{
				Log.Info ("tpsVideoView", "No");

				activity.RunOnUiThread (() => stopPlaying() );
				activity.RunOnUiThread (() => ShowImage(true) );
			}

			else if (this.IsPlaying) {
				// Video is playing

				Log.Info ("tpsVideoView", + _intervalDuration + ": " + _currentPositionSnapshot + " Video playing... percentage: " + this.BufferPercentage + " - current position:"
					+ this.CurrentPosition + " (last position; " + lastposition + ") " 
					+ (this.CurrentPosition/1000) + " (last position; " + lastposition + ") " 

					+ "- duration:" + this.Duration + "(seconds with no playback: " + _secondsWithNoPlaybackChange + ") total errors: " + _howManyErrors );

				activity.RunOnUiThread (() => ShowVideo() );

				if (_intervalDuration == 0 || lastposition == 0) {
					// there's a problem on it's way
					Log.Info ("tpsVideoView",_currentPositionSnapshot + " PROBLEM (position not changed since last check: current position:" + this.CurrentPosition + " (last position: " + lastposition + "))... (secondswithnoplayback: " + _secondsWithNoPlaybackChange + ")");
					this.Pause ();
					this.Start ();
					activity.RunOnUiThread (() => ShowSpinner() );
					_secondsWithNoPlaybackChange++;

				} else {
					// everything is good
					Log.Info ("tpsVideoView", "PLAYING... resetting secondswithnoplayback to 0");
					_secondsWithNoPlaybackChange = 0;
				}
				lastposition = this.CurrentPosition;
			}
			else
			{
				// Video is either stopped or buffering

				Log.Info ("tpsVideoView","VIDEO HAS STOPPED OR IS BUFFERING.." + this.BufferPercentage + " - current position:" + this.CurrentPosition, " Seconds no playback: " + _secondsWithNoPlaybackChange);
				///activity.RunOnUiThread (() => toastWarningMessage ("Buffering...Please wait") );

				_secondsWithNoPlaybackChange++;

				//	activity.RunOnUiThread (() => ShowImage(true) );
			}	


		}

		//		protected override void OnPause()
		//		{
		//			Log.Info ("tpsVideoView", "OnPause called");
		//			if (this != null && this.IsPlaying) {
		//				this.Pause ();
		//			}
		//			activity.RunOnUiThread (() => ShowImage() );
		//
		//			base.OnPause();
		//
		//		}
		//
		//		protected override void OnStop()
		//		{
		//			base.OnStop();
		//
		//			if (this != null)
		//			{
		//				Log.Info ("videoListView","onstop called - Stopping video");
		//				this = null;
		//			}
		//
		//			if (timer != null) {
		//				timer = null;
		//			}
		//
		//		}

		//http://developer.android.com/reference/android/widget/VideoView.html#setOnCompletionListener%28android.media.MediaPlayer.OnCompletionListener%29

		/**
		 * Register a callback to be invoked when the end of a media file has been reached during playback.
		 */

		public void OnBufferingUpdate(Android.Media.MediaPlayer mp, int extra)
		{
			Log.Info ("tpsVideoView","OnBufferingUpdateListener called...");
			activity.RunOnUiThread (() => toastWarningMessage ("Buffering updating...Please wait") );
		}

		public void OnCompletion(Android.Media.MediaPlayer mp)
		{
			Log.Info ("tpsVideoView","OnCompletion called...");
			stopPlaying ();
		}



		/**
		 * Register a callback to be invoked when an error occurs during playback or setup. If no listener is specified, or if the listener returned false, VideoView will inform the user of any errors
		 */

		public bool OnError (Android.Media.MediaPlayer mp, Android.Media.MediaError what, int extra)
		{
			Log.Info ("tpsVideoView","OnError called...");
			Log.Info ("tpsVideoView","OnError what..." + what);
			stopPlaying ();

			return true;
			//throw new NotImplementedException ();
		}


		//		public bool OnBufferingUpdate(Android.Media.MediaPlayer mp, Android.Media.MediaError what, int extra)
		//		{
		//			Log.Info ("tpsVideoView","OnBufferingUpdateListener called...");
		//			toastWarningMessage ("Buffering...Please wait");
		//
		//			return true;
		//		}

		public void RunVideo (String url)
		{
			try{
				_url = url;

				lastposition = 0;
				_secondsWithNoPlaybackChange = 0;

				activity.RunOnUiThread (() => toastWarningMessage ("Loading video...Please wait") );
				activity.RunOnUiThread (() => ShowSpinner() );

				var uri = Android.Net.Uri.Parse (url);
				this.SetVideoURI (uri);

				this.SetMediaController(mc);
				this.Visibility = ViewStates.Visible;
				SetOnErrorListener (this);
				SetOnInfoListener(this);
				SetOnPreparedListener(this);
				this.RequestFocus();


				// This prevents an error if playing is initiated before buffering
				this.PostInvalidateDelayed(100);


//				this.Prepared += (s, e) =>
//				{
//					// Start the VideoView
//					mc.SetMediaPlayer(this);
//					mc.SetAnchorView(this);
//					mc.Show();
//				};

				this.Click += (s, e) =>
				{
					this.ShowContextMenu();
				};
				StartCheckPlaybackState();
			}

			catch(Exception ex){
				Log.Info ("tpsVideoView","VideoListView crashed: url = {0}; error Message: {1}", url, ex.Message);
				ShowImage (true);
			}

		}

		private async void  StartCheckPlaybackState() 
		{
			Log.Info ("tpsVideoView","StartCheckPlaybackState called...");

			while (_videoCheckRunning) {

				await Task.Delay (1000);

				if (_secondsWithNoPlaybackChange > _timeoutSecs) 
				{
					Log.Info ("tpsVideoView", "No");

					//		activity.RunOnUiThread (() => stopPlaying() );
					activity.RunOnUiThread (() => ShowImage(true) );
				}

			}
		}

		private void stopPlaying()
		{
			Log.Info ("tpsVideoView","stopPlaying Called");
			if (this.CanPause() ) this.Pause ();
			activity.RunOnUiThread (() => ShowImage(true) );
			activity.RunOnUiThread (() => toastWarningMessage (errorMessage) );
			_videoCheckRunning = false;

		}


		public void OnPrepared (MediaPlayer mp)
		{
			mp.SetOnBufferingUpdateListener (this);
			//previewImage.setVisibility(View.GONE);
			Log.Info ("tpsVideoView","OnPrepared called");
			Start ();
		}


//		public virtual void SetOnInfoListener (Android.Media.MediaPlayer.IOnInfoListener l)
//		{
//			Log.Info ("tpsVideoView","SetOnInfoListener called");
//		}


		public bool OnInfo(Android.Media.MediaPlayer mp, Android.Media.MediaInfo what, int extra)
		{

			Log.Info ("tpsVideoView","OnInfo called");

			if (MediaInfo.VideoRenderingStart == what) {
				Log.Info ("tpsVideoView","Hiding spinner");
				activity.RunOnUiThread (() => HideSpinner() );
			}
			if (MediaInfo.BufferingStart == what) {
				Log.Info ("tpsVideoView","Showing spinner");
				// just because we are buffering we could still be playing so don't show spinner (from Steve 27/10/15)
				//	activity.RunOnUiThread (() => ShowSpinner() );
			}
			if (MediaInfo.BufferingEnd == what) {
				Log.Info ("tpsVideoView","Hiding spinner");
				activity.RunOnUiThread (() => HideSpinner() );
			}

			return true;

		}

		// layout changes

		public void loadLandscape(Android.Content.Res.Configuration newConfig)
		{
			Log.Info ("tpsVideoView","Load Landscape");

			scrollView.Visibility = ViewStates.Gone;
			heading.Visibility = ViewStates.Gone;



			// set videoview parameters
			LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams (LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.MatchParent); //Width, Height

			//set frame parameters
			videoFrame.LayoutParameters = parms;

			// set videoview parameters
			this.LayoutParameters = new FrameLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.MatchParent);


			// refresh views if not null
			if (videoFrame != null) videoFrame.Invalidate ();
			if (mainLayout != null) mainLayout.Invalidate ();


		}

		public void loadPortrait(Android.Content.Res.Configuration newConfig)
		{
			Log.Info ("tpsVideoView","Load Portrait called");

			scrollView.Visibility = ViewStates.Visible;
			heading.Visibility = ViewStates.Visible;


			LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams (LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.WrapContent); //Width, Height

			//set frame parameters
			videoFrame.LayoutParameters = parms;

			//var size = Display.GetSize ();

			this.LayoutParameters = new FrameLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.MatchParent);


			// set videoview parameters
			if (videoFrame != null) videoFrame.Invalidate ();
			if (mainLayout != null) mainLayout.Invalidate ();


		}

		public void setDimensions(int w, int h) {

			Log.Info ("tpsVideoView","Setting Dimensions");
			this.mForceHeight = h;
			this.mForceWidth = w;

		}

		protected void onMeasure(int width, int height) {

			Log.Info ("tpsVideoView","Measuring");
			SetMeasuredDimension (mForceWidth, mForceHeight);
		}


		private void toastWarningMessage(String message) 
		{
			Log.Info ("tpsVideoView","Warning");
			activity.RunOnUiThread (() => Toast.MakeText (Context, message, ToastLength.Long));
		}

		public void SurfaceCreated(ISurfaceHolder holder)
		{
			Log.Info ("tpsVideoView","Surface Created");
			mp.SetDisplay(holder);
		}
		public void SurfaceDestroyed(ISurfaceHolder holder)
		{
			Log.Info ("tpsVideoView","Surface Destroyed");
		}
		public void SurfaceChanged(ISurfaceHolder holder, Android.Graphics.Format format, int w, int h)
		{
			Log.Info ("tpsVideoView","Surface Changed");

		}
	}
}