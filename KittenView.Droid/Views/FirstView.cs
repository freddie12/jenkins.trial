using Android.App;
using Android.OS;
using Cirrious.MvvmCross.Droid.Views;
using Android.Views;
using Android.Graphics;
using Android.Widget;
using Android.Media;
using System.IO;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using KittenView.Droid.Views.Video;
using Android.Content;
using Android.Util;

namespace KittenView.Droid.Views
{
	[Activity(Label = "FirstView")]
	public class FirstView : MvxActivity
	{
		VideoView videoView;
		LinearLayout _mainLayout;

		private const string TP_STREAM = "http://mobile.tripleplay-services.com/triplecode/156_WiFi_SD/156_WiFi_SD.m3u8";
		private const string TP_STREAM_LOW = "http://mobile.tripleplay-services.com/triplecode/156_WiFi_SD/156_WiFi_SD_low_quality.m3u8";
		private const string TP_STREAM_HIGH = "http://mobile.tripleplay-services.com/triplecode/156_WiFi_SD/156_WiFi_SD_high_quality.m3u8";

		private const string VEVO_STREAM = "http://vevoplaylist-live.hls.adaptive.level3.net/vevo/ch2/appleman.m3u8";	
		private const string VEVO_STREAM_HIGH = "http://vevoplaylist-live.hls.adaptive.level3.net/vevo/ch2/06/prog_index.m3u8";

		private const string APPLE_STREAM = "http://devimages.apple.com/iphone/samples/bipbop/bipbopall.m3u8";
		private const string APPLE_STREAM_2 = "http://qthttp.apple.com.edgesuite.net/1010qwoeiuryfg/sl.m3u8";

		private const double WIDTH = 711.11111;
		private const double HEIGHT = 400;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);
			SetContentView(Resource.Layout.FirstView);
			Init ();
			Start ();
		}

		private void Start ()
		{
			//			videoView.SetVideoURI (Android.Net.Uri.Parse (Path));
			videoView.SetVideoURI (Android.Net.Uri.Parse (Path));
			videoView.Start ();
		}

		private void Init ()
		{
			FindViewReferences ();
			HideSystemUiBars (false);
			InitTouchEvents ();
		}

		private void HideSystemUiBars (bool hide)
		{
			var decorView = Window.DecorView;
			decorView.SetBackgroundColor (Color.ParseColor ("#000000"));

			var newUiOptions = 0;

			if (hide) {
				newUiOptions |= (int)SystemUiFlags.LayoutStable;
				newUiOptions |= (int)SystemUiFlags.HideNavigation;
				newUiOptions |= (int)SystemUiFlags.Fullscreen;
			}

			decorView.SystemUiVisibility = (StatusBarVisibility) newUiOptions;
			SystemToolBarHidden = hide;
		}

		private void InitTouchEvents ()
		{
			
			Window.DecorView.SetOnTouchListener (new DecorOnTouchListener (this));
			_mainLayout.Click += HandleVideoClick;
		}

		private void HandleVideoClick (object sender, EventArgs e)
		{
			HideSystemUiBars (true);
		}

		private bool SystemToolBarHidden {
			get;
			set;
		}

		private class DecorOnTouchListener : Java.Lang.Object, View.IOnTouchListener
		{
			Activity activity;
			public DecorOnTouchListener (Activity activity)
			{
				this.activity = activity;
			}

			public bool OnTouch (View sender, MotionEvent e)
			{
				return true;	
			}
		}

		private void FindViewReferences ()
		{
			videoView = FindViewById <VideoView> (Resource.Id.videoView);
			_mainLayout = FindViewById <LinearLayout> (Resource.Id.mainLayout);
		}

		private string Path {
			get;
			set;
		} = VEVO_STREAM;

		private void Write (string message)
		{
			Console.WriteLine (message);
		}
	}

}