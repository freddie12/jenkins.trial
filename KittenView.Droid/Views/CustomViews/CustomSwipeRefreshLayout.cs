﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V4.Widget;

namespace KittenView.Droid.Views.CustomViews
{
	public class CustomSwipeRefreshLayout : FrameLayout, ViewGroup.IOnHierarchyChangeListener, SwipeRefreshLayout.IOnRefreshListener
	{
		SwipeRefreshLayout emptyViewRefresher;
		SwipeRefreshLayout listViewRefresher;
		ListView mListView;
		ListState mLayoutState;

		public CustomSwipeRefreshLayout (Context context) : base (context) {}
		public CustomSwipeRefreshLayout (Context context, IAttributeSet attrs) : base (context, attrs)
		{
			var a = context.Theme.ObtainStyledAttributes (attrs, Resource.Styleable.CustomSwipeRefreshLayout, 0, 0);
			try {
//				var emptyRefresherId = a.GetResourceId (Resource.Styleable.CustomSwipeRefreshLayout_emptyRefresher, 0);
//				var listRefresherId  = a.GetResourceId (Resource.Styleable.CustomSwipeRefreshLayout_listRefresher, 0);
				var d = a.IndexCount;
				var str = a.GetString (4);
//				InflateViews (emptyRefresherId, listRefresherId); 
				InitialiseView ();
			} catch (Exception e) {}
		}

//		var a = context.Theme.ObtainStyledAttributes (attrs, Resource.Styleable.CustomLayout, 0, 0);
//		var one = a.GetResourceId (Resource.Styleable.CustomLayout_one, 0);
//		var two  = a.GetResourceId (Resource.Styleable.CustomLayout_two, 0);
		
		#region initialisers
		private void InflateViews (int emptyId, int listId)
		{
			if (emptyId == 0 || listId == 0)
				return;

			emptyViewRefresher = FindViewById<SwipeRefreshLayout> (emptyId);
			listViewRefresher = FindViewById<SwipeRefreshLayout> (listId);
		}

		private void InitialiseView ()
		{
			if (AttributesUndefined)
				return;

			var childView = GetChildListView;

			if (mListView == null || mListView.GetType () != typeof(ListView)) {
				Log.Error ("CustomSwipeLayoutError", "Child of listViewRefresher must be typeof ListView");
				return;
			}

			mListView = (ListView) childView;

			SetupListChangeListener ();
		}

		private void SetupListChangeListener ()
		{
			if (mListView == null)
				return;

			mListView.SetOnHierarchyChangeListener (this);
			UpdateState ();
		}

		private void UpdateState ()
		{
			if (mListView == null)
				return;

			if (mListView.Count == 0) {
				ShowEmptyView ();
			} else
				ShowFullView ();

			ResetRefreshers ();
		}

		private void ResetRefreshers ()
		{
			if (emptyViewRefresher !=null)
				emptyViewRefresher.Refreshing = false;

			if (listViewRefresher != null)
				listViewRefresher.Refreshing  = false;
		}

		public View GetChildListView
		{
			get { return mListView ?? LoadChildListViewFrom (listViewRefresher); }
		}

		private View LoadChildListViewFrom (ViewGroup parent)
		{
			if (parent == null)
				return null;

			return parent.GetChildAt (0);
		}

		private bool AttributesUndefined 
		{
			get { return emptyViewRefresher == null || listViewRefresher == null; }
		}
		#endregion

		#region view manipulations
		private void ShowEmptyView ()
		{
			if (mLayoutState == ListState.Empty)
				return;

			if (emptyViewRefresher != null) {
				emptyViewRefresher.Visibility = ViewStates.Visible;
				emptyViewRefresher.SetOnRefreshListener (this);
			}

			if (listViewRefresher != null) {
				listViewRefresher.Visibility = ViewStates.Gone;
				listViewRefresher.SetOnRefreshListener (null);
				mLayoutState = ListState.Full;
			}
		}

		private void ShowFullView ()
		{
			if (mLayoutState == ListState.Full)
				return;

			if (emptyViewRefresher != null) {
				emptyViewRefresher.Visibility = ViewStates.Gone;
				emptyViewRefresher.SetOnRefreshListener (null);
			}

			if (listViewRefresher != null) {
				listViewRefresher.Visibility = ViewStates.Visible;
				listViewRefresher.SetOnRefreshListener (this);
				mLayoutState = ListState.Full;
			}
		}
		#endregion

		#region listener callbacks
		public void OnChildViewAdded (View parent, View child)
		{
			UpdateState ();
		}
		public void OnChildViewRemoved (View parent, View child)
		{
			UpdateState ();
		}
		public void OnRefresh ()
		{
			UpdateState ();
		}
		#endregion
	}

	public enum ListState
	{
		Empty,
		Full
	}
}