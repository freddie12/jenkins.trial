﻿using System;

namespace KittenView.Core
{
	public class Kitten 
	{

		public string Name { get; set; }
		public int Price { get; set; }
		public string ImageUrl { get; set; }

	}
}

