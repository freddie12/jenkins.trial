﻿using System;
using System.Collections.Generic;

namespace KittenView.Core
{
	public class KittenGenesisService : IKittenGenesisService
	{

		private readonly List<string> _names = new List<string>() {
			"Tiddles",
			"Max",
			"Charlotte",
			"Dave",
			"Alex",
			"Handz",
			"Orlo",
			"Freddie"
		};

		public Kitten CreateNewKitten (string extra = "" ) 
		{
			return new Kitten () {
				Name = _names[Random(_names.Count)] + " " + extra, 
				ImageUrl = string.Format("http://placekitten.com/{0}/{0}", Random(20) + 300), 
				Price = RandomPrice()
			};
		}

		private readonly System.Random _random = new System.Random ();
		protected int Random(int count)
		{
			return _random.Next (count);
		}

		public int RandomPrice()
		{
			return Random (23) + 3;
		}
	}



}

