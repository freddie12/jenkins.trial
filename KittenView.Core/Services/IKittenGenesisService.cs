﻿using System;

namespace KittenView.Core
{
	public interface IKittenGenesisService 
	{
		Kitten CreateNewKitten (string extra = "");
	}

}

