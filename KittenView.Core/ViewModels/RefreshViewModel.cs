using Cirrious.MvvmCross.ViewModels;
using System.Collections.Generic;

namespace KittenView.Core.ViewModels
{
    public class RefreshViewModel : MvxViewModel
    {
		private List<string> _list = new List<string> () {"New string", "New string"}; 
		public List<string> List
		{
			get { return _list; }
			set { _list = value; }
		}

		public RefreshViewModel ()
		{
		}
    }
}
